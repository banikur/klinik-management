/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80030 (8.0.30)
 Source Host           : localhost:3306
 Source Schema         : clinicmanagement

 Target Server Type    : MySQL
 Target Server Version : 80030 (8.0.30)
 File Encoding         : 65001

 Date: 28/03/2024 22:14:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for autentikasi
-- ----------------------------
DROP TABLE IF EXISTS `autentikasi`;
CREATE TABLE `autentikasi`  (
  `ID_Pengguna` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Nama_Lengkap` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Role` enum('Admin','Farmasi','Dokter') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Pengguna`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of autentikasi
-- ----------------------------
INSERT INTO `autentikasi` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Admin');
INSERT INTO `autentikasi` VALUES (2, 'farmasi', '21232f297a57a5a743894a0e4a801fc3', 'farmasi', 'Farmasi');
INSERT INTO `autentikasi` VALUES (3, 'dokter', '21232f297a57a5a743894a0e4a801fc3', 'dokter 1', 'Dokter');

-- ----------------------------
-- Table structure for jadwal_dokter
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_dokter`;
CREATE TABLE `jadwal_dokter`  (
  `ID_Jadwal` int NOT NULL,
  `ID_Pegawai` int NULL DEFAULT NULL,
  `Hari` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Jam_Mulai` time NULL DEFAULT NULL,
  `Jam_Selesai` time NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Jadwal`) USING BTREE,
  INDEX `ID_Pegawai`(`ID_Pegawai` ASC) USING BTREE,
  CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of jadwal_dokter
-- ----------------------------

-- ----------------------------
-- Table structure for mst_obat
-- ----------------------------
DROP TABLE IF EXISTS `mst_obat`;
CREATE TABLE `mst_obat`  (
  `ID_Obat` int NOT NULL AUTO_INCREMENT,
  `Nama_Obat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `Harga` decimal(10, 2) NULL DEFAULT NULL,
  `Stok` int NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Obat`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mst_obat
-- ----------------------------

-- ----------------------------
-- Table structure for mst_pasien
-- ----------------------------
DROP TABLE IF EXISTS `mst_pasien`;
CREATE TABLE `mst_pasien`  (
  `ID_Pasien` int NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Nomor_Telepon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Tanggal_Lahir` date NULL DEFAULT NULL,
  `Jenis_Kelamin` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Pasien`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mst_pasien
-- ----------------------------

-- ----------------------------
-- Table structure for mst_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `mst_pegawai`;
CREATE TABLE `mst_pegawai`  (
  `ID_Pegawai` int NOT NULL,
  `Nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Nomor_Telepon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Jabatan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Pegawai`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mst_pegawai
-- ----------------------------

-- ----------------------------
-- Table structure for pemeriksaan_medis
-- ----------------------------
DROP TABLE IF EXISTS `pemeriksaan_medis`;
CREATE TABLE `pemeriksaan_medis`  (
  `ID_Pemeriksaan` int NOT NULL,
  `ID_Pasien` int NULL DEFAULT NULL,
  `ID_Pegawai` int NULL DEFAULT NULL,
  `Tanggal_Pemeriksaan` date NULL DEFAULT NULL,
  `Diagnosis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `ID_Obat` int NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Pemeriksaan`) USING BTREE,
  INDEX `ID_Pasien`(`ID_Pasien` ASC) USING BTREE,
  INDEX `ID_Pegawai`(`ID_Pegawai` ASC) USING BTREE,
  INDEX `ID_Obat`(`ID_Obat` ASC) USING BTREE,
  CONSTRAINT `pemeriksaan_medis_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pemeriksaan_medis_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pemeriksaan_medis_ibfk_3` FOREIGN KEY (`ID_Obat`) REFERENCES `mst_obat` (`ID_Obat`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pemeriksaan_medis
-- ----------------------------

-- ----------------------------
-- Table structure for pendaftaran_pasien
-- ----------------------------
DROP TABLE IF EXISTS `pendaftaran_pasien`;
CREATE TABLE `pendaftaran_pasien`  (
  `ID_Pendaftaran` int NOT NULL,
  `ID_Pasien` int NULL DEFAULT NULL,
  `Tanggal_Pendaftaran` date NULL DEFAULT NULL,
  `ID_Pegawai` int NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Pendaftaran`) USING BTREE,
  INDEX `ID_Pasien`(`ID_Pasien` ASC) USING BTREE,
  INDEX `ID_Pegawai`(`ID_Pegawai` ASC) USING BTREE,
  CONSTRAINT `pendaftaran_pasien_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pendaftaran_pasien_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pendaftaran_pasien
-- ----------------------------

-- ----------------------------
-- Table structure for rekam_medis
-- ----------------------------
DROP TABLE IF EXISTS `rekam_medis`;
CREATE TABLE `rekam_medis`  (
  `ID_Rekam_Medis` int NOT NULL,
  `ID_Pasien` int NULL DEFAULT NULL,
  `ID_Pegawai` int NULL DEFAULT NULL,
  `Tanggal` date NULL DEFAULT NULL,
  `Catatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  PRIMARY KEY (`ID_Rekam_Medis`) USING BTREE,
  INDEX `ID_Pasien`(`ID_Pasien` ASC) USING BTREE,
  INDEX `ID_Pegawai`(`ID_Pegawai` ASC) USING BTREE,
  CONSTRAINT `rekam_medis_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rekam_medis_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rekam_medis
-- ----------------------------

-- ----------------------------
-- Table structure for resep_obat
-- ----------------------------
DROP TABLE IF EXISTS `resep_obat`;
CREATE TABLE `resep_obat`  (
  `ID_Resep` int NOT NULL,
  `ID_Pemeriksaan` int NULL DEFAULT NULL,
  `ID_Obat` int NULL DEFAULT NULL,
  `Jumlah` int NULL DEFAULT NULL,
  PRIMARY KEY (`ID_Resep`) USING BTREE,
  INDEX `ID_Pemeriksaan`(`ID_Pemeriksaan` ASC) USING BTREE,
  INDEX `ID_Obat`(`ID_Obat` ASC) USING BTREE,
  CONSTRAINT `resep_obat_ibfk_1` FOREIGN KEY (`ID_Pemeriksaan`) REFERENCES `pemeriksaan_medis` (`ID_Pemeriksaan`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `resep_obat_ibfk_2` FOREIGN KEY (`ID_Obat`) REFERENCES `mst_obat` (`ID_Obat`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resep_obat
-- ----------------------------

-- ----------------------------
-- View structure for report_jumlah_pemeriksaan_tiap_dokter
-- ----------------------------
DROP VIEW IF EXISTS `report_jumlah_pemeriksaan_tiap_dokter`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `report_jumlah_pemeriksaan_tiap_dokter` AS select `p`.`Nama` AS `Nama_Dokter`,count(0) AS `Jumlah_Pemeriksaan` from (`pemeriksaan_medis` `pm` join `mst_pegawai` `p` on((`pm`.`ID_Pegawai` = `p`.`ID_Pegawai`))) group by `pm`.`ID_Pegawai`;

-- ----------------------------
-- View structure for report_jumlah_pendaftaran_pasien_bulanan
-- ----------------------------
DROP VIEW IF EXISTS `report_jumlah_pendaftaran_pasien_bulanan`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `report_jumlah_pendaftaran_pasien_bulanan` AS select month(`pendaftaran_pasien`.`Tanggal_Pendaftaran`) AS `Bulan`,count(0) AS `Jumlah_Pendaftaran` from `pendaftaran_pasien` group by month(`pendaftaran_pasien`.`Tanggal_Pendaftaran`);

-- ----------------------------
-- View structure for report_kunjungan_rekam_medis_pasien
-- ----------------------------
DROP VIEW IF EXISTS `report_kunjungan_rekam_medis_pasien`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `report_kunjungan_rekam_medis_pasien` AS select `pm`.`ID_Pasien` AS `ID_Pasien`,`p`.`Nama` AS `Nama_Pasien`,count(0) AS `Jumlah_Pemeriksaan` from (`pemeriksaan_medis` `pm` join `mst_pasien` `p` on((`pm`.`ID_Pasien` = `p`.`ID_Pasien`))) group by `pm`.`ID_Pasien`,`p`.`Nama`;

-- ----------------------------
-- View structure for report_stock_obat
-- ----------------------------
DROP VIEW IF EXISTS `report_stock_obat`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `report_stock_obat` AS select `mst_obat`.`Nama_Obat` AS `Nama_Obat`,`mst_obat`.`Stok` AS `Stok` from `mst_obat` order by `mst_obat`.`Stok` desc;

SET FOREIGN_KEY_CHECKS = 1;
